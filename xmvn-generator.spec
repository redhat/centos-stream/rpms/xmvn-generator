%global debug_package %{nil}
%bcond_with bootstrap

Name:           xmvn-generator
Version:        1.2.2
Release:        8%{?dist}
Summary:        RPM dependency generator for Java
License:        Apache-2.0
URL:            https://github.com/fedora-java/xmvn-generator
ExclusiveArch:  %{java_arches}

Source0:        https://github.com/fedora-java/xmvn-generator/archive/refs/tags/%{version}.tar.gz#/%{name}-%{version}.tar.gz

Patch:          0001-Use-OpenJDK-21.patch
Patch:          0002-Enable-JPMS-provides-and-JAR-transformer.patch
Patch:          0003-Add-commons-io-to-classpath.patch

BuildRequires:  gcc
BuildRequires:  rpm-devel
%if %{with bootstrap}
BuildRequires:  javapackages-bootstrap
%else
BuildRequires:  maven-local
BuildRequires:  mvn(org.apache.commons:commons-compress)
BuildRequires:  mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires:  mvn(org.easymock:easymock)
BuildRequires:  mvn(org.junit.jupiter:junit-jupiter)
BuildRequires:  mvn(org.ow2.asm:asm)
%endif

Requires:       rpm-build
Requires:       lujavrite
Requires:       java-21-openjdk-headless

%description
XMvn Generator is a dependency generator for RPM Package Manager
written in Java and Lua, that uses LuJavRite library to call Java code
from Lua.

%{?javadoc_package}

%prep
%autosetup -p1
%mvn_file : %{name}

%build
%mvn_build

%install
%mvn_install
install -D -p -m 644 src/main/lua/xmvn-generator.lua %{buildroot}%{_rpmluadir}/xmvn-generator.lua
install -D -p -m 644 src/main/rpm/macros.xmvngen %{buildroot}%{_rpmmacrodir}/macros.xmvngen
install -D -p -m 644 src/main/rpm/macros.xmvngenhook %{buildroot}%{_sysconfdir}/rpm/macros.xmvngenhook
install -D -p -m 644 src/main/rpm/xmvngen.attr %{buildroot}%{_fileattrsdir}/xmvngen.attr

%files -f .mfiles
%{_rpmluadir}/*
%{_rpmmacrodir}/*
%{_fileattrsdir}/*
%{_sysconfdir}/rpm/*
%license LICENSE NOTICE
%doc README.md

%changelog
* Fri Jan 17 2025 Mikolaj Izdebski <mizdebsk@redhat.com> - 1.2.2-8
- Rebuild

* Fri Jan 17 2025 Mikolaj Izdebski <mizdebsk@redhat.com> - 1.2.2-7
- Use autosetup

* Fri Jan 17 2025 Mikolaj Izdebski <mizdebsk@redhat.com> - 1.2.2-6
- Add commons-io to classpath

* Tue Oct 29 2024 Troy Dawson <tdawson@redhat.com> - 1.2.2-5
- Bump release for October 2024 mass rebuild:
  Resolves: RHEL-64018

* Mon Aug 05 2024 Mikolaj Izdebski <mizdebsk@redhat.com> - 1.2.2-4
- Switch to Java 21 for runtime
- Resolves: RHEL-52714

* Thu Aug 01 2024 Troy Dawson <tdawson@redhat.com> - 1.2.2-4
- Bump release for Aug 2024 java mass rebuild

* Mon Jun 24 2024 Troy Dawson <tdawson@redhat.com> - 1.2.2-3
- Bump release for June 2024 mass rebuild

* Sat Jan 27 2024 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Mon Dec 11 2023 Mikolaj Izdebski <mizdebsk@redhat.com> - 1.2.2-1
- Update to upstream version 1.2.2

* Wed Aug 30 2023 Mikolaj Izdebski <mizdebsk@redhat.com> - 1.2.1-4
- Enable JPMS provides and JAR transformation in ELN

* Sat Jul 22 2023 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_39_Mass_Rebuild

* Fri Mar 17 2023 Mikolaj Izdebski <mizdebsk@redhat.com> - 1.2.1-2
- Enable javadoc package

* Mon Mar 13 2023 Mikolaj Izdebski <mizdebsk@redhat.com> - 1.2.1-1
- Update to upstream version 1.2.1

* Fri Mar 10 2023 Mikolaj Izdebski <mizdebsk@redhat.com> - 1.2.0-1
- Update to upstream version 1.2.0

* Mon Mar 06 2023 Mikolaj Izdebski <mizdebsk@redhat.com> - 1.1.0-1
- Update to upstream version 1.1.0

* Mon Mar 06 2023 Mikolaj Izdebski <mizdebsk@redhat.com> - 1.0.0-1
- Initial packaging
